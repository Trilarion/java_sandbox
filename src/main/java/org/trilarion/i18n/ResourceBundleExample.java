package org.trilarion.i18n;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Example for using a resource bundle to get localized strings.
 */
public class ResourceBundleExample {

    public static void main (String[] args) throws IOException {
        System.out.println("Current Locale: " + Locale.getDefault());
        create_resources();

        ResourceBundle bundle = ResourceBundle.getBundle("i18n", Locale.forLanguageTag("de"));
        System.out.println(bundle.getString("key")); // should print Auto
        System.out.println(bundle.getString("rare key")); // should print ship
    }

    public static void create_resources() throws IOException {
        Properties properties = new Properties();
        properties.put("key", "car");
        properties.put("another key", "cat");
        properties.put("rare key", "ship");
        FileOutputStream outputStream = new FileOutputStream("src/main/resources/i18n_en.properties");
        properties.store(outputStream, "Comment");
        outputStream.flush();
        outputStream.close();

        properties.put("key", "Auto");
        properties.put("another key", "Katze");
        outputStream = new FileOutputStream("src/main/resources/i18n_de.properties");
        properties.store(outputStream, "Comment");
        outputStream.flush();
        outputStream.close();
    }

}
